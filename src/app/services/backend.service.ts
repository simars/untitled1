import { Injectable } from '@angular/core';
import { Observable, throwError, of } from 'rxjs';
import {delay, tap, map, catchError} from 'rxjs/operators';

export interface User {
  id: number;
  name: string;
}

export interface Ticket {
  id: number;
  description: string;
  assigneeId: number;
  completed: boolean;
}

function randomDelay() {
  return Math.random() * 4000;
}

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  storedTickets: Ticket[] = [
    {
      id: 0,
      description: 'Install a monitor arm',
      assigneeId: 111,
      completed: false
    },
    {
      id: 1,
      description: 'Move the desk to the new location',
      assigneeId: null,
      completed: false
    }
  ];

  ticketsBy(term: string) {
    console.log('term');
    if (!term) {
      return this.tickets();
    }
    return this.tickets().
    pipe(map((f: Array<Ticket>) => f.filter(t => t.description.indexOf(term) !== -1)));
  }

  storedUsers: User[] = [{ id: 111, name: 'Victor' }];

  lastId = 1;

  constructor() { }

  private findTicketById = id =>
    this.storedTickets.find(ticket => ticket.id === +id)
  private findUserById = id => this.storedUsers.find(user => user.id === +id);

  tickets() {
    return of(this.storedTickets).pipe(delay(randomDelay()));
  }

  ticket(id: number): Observable<Ticket> {
    return of(this.findTicketById(id)).pipe(
      tap(t => {if (!t) { throw new Error(`Ticket not found with id ${id}`); }}),
      delay(randomDelay())
    );
  }

  users() {
    return of(this.storedUsers).pipe(delay(randomDelay()));
  }

  user(id: number) {
    return of(this.findUserById(id)).pipe(
      tap(t => {if (!t) { throw new Error(`User not found with id ${id}`); }}),
      delay(randomDelay())
    );
  }

  newTicket(payload: { description: string }) {
    const newTicket: Ticket = {
      id: ++this.lastId,
      description: payload.description,
      assigneeId: null,
      completed: false
    };

    return of(newTicket).pipe(
      delay(randomDelay()),
      tap((ticket: Ticket) => this.storedTickets.push(ticket))
    );
  }

  assign(ticketId: number, userId: number) {
    const foundTicket = this.findTicketById(+ticketId);
    const user = this.findUserById(+userId);
    if (foundTicket && user) {
      return of(foundTicket).pipe(
        delay(randomDelay()),
        tap((ticket: Ticket) => {
          ticket.assigneeId = +userId;
        })
      );
    }

    return throwError(new Error('ticket or user not found'));
  }

  complete(ticketId: number, completed: boolean) {
    const foundTicket = this.findTicketById(+ticketId);
    if (foundTicket) {
      return of(foundTicket).pipe(
        delay(randomDelay()),
        tap((ticket: Ticket) => {
          ticket.completed = completed;
        })
      );
    }

    return throwError(new Error('ticket not found'));
  }
}
