import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {BackendService, Ticket, User} from './backend.service';
import {catchError, concatMap, finalize, map, switchMap, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  private subject = new BehaviorSubject<Ticket[]>([]);

  private _userSubject = new BehaviorSubject<User[]>([]);

  public tickets$ = this.subject.asObservable().pipe(
    tap(console.log)
  );

  public user$ = this._userSubject.asObservable();

  constructor(private backend: BackendService) {
    this.reload();
  }

  public loading$ = new BehaviorSubject<boolean>(false);

  reload() {
    this.backend.tickets().subscribe(
      next => this.subject.next(next)
    );
    this.backend.users().subscribe(
      next => this._userSubject.next(next)
    );
  }

  filter(term: string): Observable<Ticket[]> {
    this.loading$.next(true);
    return this.backend.ticketsBy(term).pipe(
      finalize(() => this.loading$.next(false))
    );
  }


  selectTicketById(id: number): Observable<Ticket> {
    const tickets = this.subject.getValue();
    const index = tickets.findIndex(t => t.id === +id);
    return index >= 0 ? of(tickets[index]) : this.backend.ticket(id).pipe(
      tap(t => this.subject.next([...tickets, t])),
    );
  }

  createTicket(value: Ticket): Observable<Ticket> {
    return this.backend.newTicket(value)
      .pipe(
        map(v => ({ ...value, ...v})),
        catchError((err) => of({...value, error: err.message})),
        tap(this.publish)
      );
  }

  publish = (newTicket: Ticket) => {
    const tickets = this.subject.getValue();
    const ticketIndex = newTicket.id == null ? -1  : tickets.findIndex(t => t.id === +newTicket.id);
    const newTickets =  [...tickets];
    if ( ticketIndex < 0 ) {
      newTickets.push(newTicket);
    } else {
      newTickets[ticketIndex] = newTicket;
    }
    this.subject.next(newTickets);
  }

  saveTicket(ticket: Ticket) {
    return of({...ticket, error: null}).pipe(
      tap(() => this.loading$.next(true)),
      concatMap(value => value.id >= 0 ? this.selectTicketById(value.id) :  this.createTicket(value)),
      switchMap(value => this.backend.assign(value.id, ticket.assigneeId)
        .pipe(
          map(v => ({...value, ...v, error: null})),
          catchError(err => of({...value, error: err.message}))
        )
      ),
      switchMap(value => this.backend.complete(value.id, ticket.completed)
        .pipe(
          map(v => ({...value, ...v})),
          catchError(err => of({...value, error: err.message}))
        )
      ),
      tap(this.publish),
      finalize(() => this.loading$.next(false)),
    );
  }

}
