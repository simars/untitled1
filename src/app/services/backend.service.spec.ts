import { TestBed } from '@angular/core/testing';
import { BackendService, Ticket, User } from './backend.service';

describe('BackendService', () => {
  let backendService: BackendService;
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [BackendService] });
    backendService = TestBed.get(BackendService);
  });

  it('should create an instance', () => {
    expect(backendService).toBeDefined();
  });

  it('should return 2 tickets', () => {
    backendService.tickets().subscribe((tickets: Ticket[]) => {
      expect(tickets.length).toBe(2);
    });
  });

  it('should return 1 user', () => {
    backendService.users().subscribe((users: User[]) => {
      expect(users.length).toBe(2);
    });
  });

  it('should add a new ticket', () => {
    backendService.newTicket({ description: 'New Ticket' }).subscribe((ticket: Ticket) => {
      expect(ticket.description).toBe('New Ticket');
    });
  });
});
