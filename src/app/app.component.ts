import { Component } from '@angular/core';
import {StoreService} from './services/store.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  loading$: Observable<boolean>;

  constructor(store: StoreService) {
    store.reload();
    this.loading$ = store.loading$;
  }

}
