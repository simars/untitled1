// ticket.reducers.ts
import * as fromActions from './ticket.actions';
import {Ticket} from '../../services/backend.service';

export interface TicketReducerState {
  error?: string;
  loaded?: boolean;
  loading?: boolean;
  state?: Ticket[];
}

export const initialState: TicketReducerState = {
  error: '',
  loaded: false,
  loading: false,
  state: [],
};

export function ticketReducer(reducerState: TicketReducerState = initialState, action) {

  switch (action.type) {
    case fromActions.ADD_TICKET_SUCCESS: {
      const data = [...reducerState.state, action.payload];
      return { ...reducerState, state: data };
    }

    case fromActions.REMOVE_TICKET_SUCCESS: {
      const data = reducerState.state.filter(
        ticket => ticket.id !== action.payload.id
      );
      return { ...reducerState, state: data };
    }

    case fromActions.UPDATE_STATUS: {
      return {...reducerState, ...action.payload };
    }
  }

  return reducerState;
}
