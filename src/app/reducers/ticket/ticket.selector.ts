import {createFeatureSelector, createSelector} from '@ngrx/store';
import {TicketReducerState} from './ticket.reducers';
import {ticketStateConfig} from './ticket.state';


export const getTicketReducerState = createFeatureSelector<TicketReducerState>(ticketStateConfig.stateKey);

export const getTickets = createSelector(getTicketReducerState, (ticketReducerState) => ticketReducerState.state);
