import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {BackendService} from '../../services/backend.service';
import * as fromTicketActions from './ticket.actions';
import {catchError, concatMap, exhaustMap, filter, map, withLatestFrom} from 'rxjs/operators';
import {of} from 'rxjs';
import {ROUTER_NAVIGATION} from '@ngrx/router-store';
import {MergedRouteNavigationAction} from '../router';
import {select, Store} from '@ngrx/store';
import {TicketReducerState} from './ticket.reducers';
import {getTicketReducerState} from './ticket.selector';
import {ADD_TICKET_SUCCESS, AddTicket} from './ticket.actions';

// import {RouterNavigationAction, ROUTER_NAVIGATION, RouterNavigationPayload } from '../router';

@Injectable()
export class TicketsEffects {

  constructor(
    private actions$: Actions,
    private ticketsService: BackendService,
    private store: Store<{ticket: TicketReducerState}>
  ) {}


  @Effect()
  getTickets$ = this.actions$.pipe(
    ofType(ROUTER_NAVIGATION),
    map( (action: MergedRouteNavigationAction) => action.payload.routerState.url),
    filter((url: string)  => url.startsWith('/tickets')),
    withLatestFrom(this.store.pipe(select(getTicketReducerState))),
    filter(([url, ticketState]) => !ticketState.loaded ),
    exhaustMap(() =>
      this.ticketsService
        .tickets().pipe(
        map(tickets => new fromTicketActions.UpdateStatus({state: tickets, loaded: true, loading: false, error: ''}))
        , catchError(error => of(new fromTicketActions.UpdateStatus({ error})))
    )
    ));

  @Effect()
  addTicket$ = this.actions$.pipe(
    ofType(ADD_TICKET_SUCCESS),
    map((action: AddTicket)  => action.payload),
    concatMap((description) => this.ticketsService.newTicket({description})
      .pipe(
        map( ticket => new fromTicketActions.AddTicketSuccess(ticket)),
        catchError(error => of(new fromTicketActions.UpdateStatus({error})))
      )
    )
  );

}
