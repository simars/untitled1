import {NgModule} from '@angular/core';
import {StoreModule} from '@ngrx/store';
import {ticketReducer} from './ticket.reducers';
import {EffectsModule} from '@ngrx/effects';
import {TicketsEffects} from './ticket.effects';
import {ticketStateConfig} from './ticket.state';

@NgModule({
  imports: [
    StoreModule.forFeature(ticketStateConfig.stateKey, ticketReducer ),
    EffectsModule.forFeature([TicketsEffects])
  ]
})
export class NgrxTicketModule {

}
