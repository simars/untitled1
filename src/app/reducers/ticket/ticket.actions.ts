// todo.actions.ts
import { Action } from '@ngrx/store';
import {Ticket} from '../../services/backend.service';
import {TicketReducerState} from './ticket.reducers';

export const ADD_TICKET_SUCCESS = '[Ticket] Add Ticket Success';
export const REMOVE_TICKET_SUCCESS = '[Ticket] Remove Ticket Success';
export const UPDATE_STATUS = '[Ticket] Update Status';
export const ADD_TICKET = '[Ticket] Request Add Ticket';
export const REMOVE_TICKET = '[Ticket] Request Remove Ticket';

export class AddTicket implements Action {
  readonly type = ADD_TICKET;
  constructor(public payload: string) {}
}

export class RemoveTicket implements Action {
  readonly type = REMOVE_TICKET;
  constructor(public payload: Ticket | number) {}
}

export class UpdateStatus implements Action {
  readonly type = UPDATE_STATUS;
  constructor(public payload: TicketReducerState) {}
}

export class AddTicketSuccess implements Action {
  readonly type = ADD_TICKET_SUCCESS;
  constructor(public payload: Ticket) {}
}

export class RemoveTicketSuccess implements Action {
  readonly type = REMOVE_TICKET_SUCCESS;
  constructor(public payload: Ticket | number) {}
}


// exporting a custom type
export type TicketActions = AddTicket | RemoveTicket | UpdateStatus | AddTicketSuccess | RemoveTicketSuccess;
