// todo.actions.ts
import { Action } from '@ngrx/store';
import {UiState} from './ui.reducers';

export const UPDATE = '[UI] Update';
export const RESET = '[UI] Reset';



export class Update implements Action {
  readonly type = UPDATE;
  constructor(public payload: UiState) {}
}

export class Reset implements Action {
  type = RESET;
}


// exporting a custom type
export type UiActions = Update | Reset;
