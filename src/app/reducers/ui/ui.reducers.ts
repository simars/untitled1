// ticket.reducers.ts
import * as fromActions from './ui.actions';

export interface UiState {
  busy?: boolean;
}

export interface UiReducerState {
  state: UiState;
}

export const initialState: UiReducerState = {
  state: {
    busy: false
  },
};

export function uiReducer(reducerState: UiReducerState = initialState, action) {

  switch (action.type) {
    case fromActions.UPDATE: {
      return { ...reducerState, state: action.payload };
    }
  }

  return reducerState;
}
