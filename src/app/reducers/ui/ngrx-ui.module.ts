import {NgModule} from '@angular/core';
import {StoreModule} from '@ngrx/store';
import {uiReducer} from './ui.reducers';

const config = {
  stateKey: 'ui'
};

@NgModule({
  imports: [
    StoreModule.forFeature(config.stateKey, uiReducer )
  ]
})
export class NgrxUiModule {

}
