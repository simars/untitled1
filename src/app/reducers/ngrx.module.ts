import {ActionReducerMap, MetaReducer, StoreModule} from '@ngrx/store';
import {storeFreeze} from 'ngrx-store-freeze';
import {NgModule} from '@angular/core';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../../environments/environment';
import {NgrxUiModule} from './ui/ngrx-ui.module';
import {EffectsModule} from '@ngrx/effects';
import {NgrxTicketModule} from './ticket';
import {NgrxRouterStoreModule} from './router';


export const metaReducers: MetaReducer<{}>[] =
  !environment.production ? [storeFreeze] : [];



@NgModule({
  imports: [
    StoreModule.forRoot({} as ActionReducerMap<{}>, { metaReducers }),
    EffectsModule.forRoot([]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    NgrxRouterStoreModule,
    NgrxTicketModule,
    NgrxUiModule
  ]
})
export class NgrxAppModule {
}








