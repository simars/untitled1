import {Component, Input} from '@angular/core';
import {Ticket} from '../../services/backend.service';

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.css']
})
export class TicketListComponent {


  @Input('tickets') tickets: Ticket[];


  trackByFn(index, item) {
    return item.id;
  }

}
