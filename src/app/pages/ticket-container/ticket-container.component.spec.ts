import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketContainerComponent } from './ticket-container.component';
import {By} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';
import {Subject} from 'rxjs';
import {Ticket} from '../../services/backend.service';
import {Store} from '@ngrx/store';

describe('TicketContainerComponent', () => {
  let component: TicketContainerComponent;
  let fixture: ComponentFixture<TicketContainerComponent>;
  let subject: Subject<Ticket[]>;

  beforeEach(async () => {
    subject = new Subject();
    await TestBed.configureTestingModule({
      declarations: [ TicketContainerComponent ],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {
          provide: Store,
          useValue: {
            pipe: jasmine.createSpy('pipe').and.returnValue(subject),
            dispatch: jasmine.createSpy('dispatch')
          }
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', async () => {
    expect(component).toBeTruthy();
  });


  it('should show title', async () => {
    expect(component).toBeTruthy();
    const de = fixture.debugElement.query(By.css('h2'));
    const el = de.nativeElement as HTMLElement;
    await expect(el.innerText).toContain('Tickets');
  });


  it('it should show loading message when loading Tickets', async () => {
    expect(component).toBeTruthy();
    const de = fixture.debugElement.query(By.css('ul'));
    const el = de.nativeElement as HTMLElement;
    await expect(el.innerText).toContain('Loading Tickets');
  });

  it('It should not display any loading message when tickets have been emitted', async () => {
    expect(component).toBeTruthy();
    subject.next([{
      id: 1,
      description: '?',
      assigneeId: 1,
      completed: false
    }]);
    fixture.detectChanges();
    await fixture.whenStable();
    const el = fixture.debugElement.nativeElement as HTMLElement;
    await expect(el.innerText).not.toContain('Loading Tickets');
  });


});
