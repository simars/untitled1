import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TicketContainerComponent} from './ticket-container.component';
import {By} from '@angular/platform-browser';
import {Subject} from 'rxjs';
import {Ticket} from '../../services/backend.service';
import {Store} from '@ngrx/store';
import {TicketListComponent} from '../ticket-list/ticket-list.component';
import {TicketSearchBoxComponent} from '../ticket-search-box/ticket-search-box.component';
import {RouterTestingModule} from '@angular/router/testing';

describe('TicketContainerComponent', () => {
  let component: TicketContainerComponent;
  let fixture: ComponentFixture<TicketContainerComponent>;
  let subject: Subject<Ticket[]>;

  beforeEach(async () => {
    subject = new Subject();
    await TestBed.configureTestingModule({
      declarations: [ TicketContainerComponent, TicketListComponent, TicketSearchBoxComponent ],
      providers: [
        {
          provide: Store,
          useValue: {
            pipe: jasmine.createSpy('pipe').and.returnValue(subject),
            dispatch: jasmine.createSpy('dispatch')
          }
        }
      ],
      imports: [
        RouterTestingModule
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('It should pass tickets fetched from store as input to app-ticket-list component', async () => {
    expect(component).toBeTruthy();
    const tickets = [{
      id: 1,
      description: '?',
      assigneeId: 1,
      completed: false
    }];
    subject.next(tickets);
    fixture.detectChanges();
    await fixture.whenStable();
    const ticketListComponent = fixture.debugElement.query(By.css('app-ticket-list')).componentInstance as TicketListComponent;
    await expect(ticketListComponent.tickets[0]).toBe(tickets[0]);
  });

  it('It should handle search events from  app-ticket-search-box component', async () => {
    expect(component).toBeTruthy();
    const tickets = [{
      id: 1,
      description: '?',
      assigneeId: 1,
      completed: false
    }];
    subject.next(tickets);
    fixture.detectChanges();
    await fixture.whenStable();
    const searchBoxComponent = fixture.debugElement.query(By.css('app-ticket-search-box')).componentInstance as TicketSearchBoxComponent;
    spyOn(component, 'handleSearch');
    const searchCriterion = {term: '?', completedOnly: false};
    searchBoxComponent.onSearch.emit(searchCriterion);
    await expect(component.handleSearch).toHaveBeenCalledWith(searchCriterion);
  });


});
