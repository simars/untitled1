import {Component, OnInit} from '@angular/core';
import {Ticket} from '../../services/backend.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';
import {select, Store} from '@ngrx/store';
import {TicketReducerState} from '../../reducers/ticket/ticket.reducers';
import {TicketSearchCriterion} from '../ticket-search-box/ticket-search-box.component';
import {getTickets} from '../../reducers/ticket';

@Component({
  selector: 'app-ticket-container',
  templateUrl: './ticket-container.component.html',
  styleUrls: ['./ticket-container.component.css']
})
export class TicketContainerComponent implements OnInit {


  private subject = new BehaviorSubject<TicketSearchCriterion>({term: '', completedOnly: false});

  private searchCriterion$ = this.subject.asObservable();

  tickets$: Observable<Ticket[]>;

  constructor(private store: Store<{ticket: TicketReducerState}>) {}

  ngOnInit() {
    this.tickets$ = this.store.pipe(select(getTickets))
      .pipe(
        switchMap((tickets) => (this.searchCriterion$).pipe(
          map(
            ({term, completedOnly}) => tickets.filter(
              t => (!completedOnly || t.completed) && (!term || t.description.indexOf(term) !== -1))
          )
          )
        ),
      );
  }

  handleSearch(searchCriterion: TicketSearchCriterion) {
    this.subject.next(searchCriterion);
  }

}
