import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {concat, Observable, of} from 'rxjs';
import {catchError, concatMap, exhaustMap, flatMap, map, switchMap, tap} from 'rxjs/operators';
import {BackendService, Ticket, User} from '../../services/backend.service';
import {FormBuilder, FormControl, FormGroup, NgForm} from '@angular/forms';
import {StoreService} from '../../services/store.service';

@Component({
  selector: 'app-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.css']
})
export class TicketDetailComponent implements OnInit {

  form: FormGroup;
  ticket$: Observable<any>;
  users$: Observable<User[]>;
  private _editing: boolean;

  constructor(
    private fb: FormBuilder,
    activatedRoute: ActivatedRoute,
              public store: StoreService,
              private router: Router) {

    this.form = fb.group({
      id: new FormControl({value: undefined, disabled: true}),
      description: [''],
      assigneeId: [undefined],
      completed: [false],
      error: new FormControl({value: '', disabled: true})
    });

    this.users$ = store.user$;

    this.ticket$ =
      activatedRoute.params.pipe(
        map(p => p.id),
        tap(id =>  this.editing = id === 'new'),
        switchMap(id => id === 'new' ? of(this.form.getRawValue()) : this.store.selectTicketById(+id)),
        catchError(error => of({error})),
        tap(value => this.form.patchValue({...value}))
      );
  }

  set editing(edit: boolean) {
    const action = edit ? 'enable' : 'disable';
    ['description', 'completed', 'assigneeId'].forEach(key => this.form.controls[key][action]());
    this._editing = edit;
  }

  get editing() {
    return this._editing;
  }

  onSubmit(form: FormGroup) {
    this.store.saveTicket(form.getRawValue())
      .pipe(
        tap(value => value && this.form.patchValue(value))
      )
      .subscribe(
      (ticket) => this.router.navigate(['/ticket', +ticket.id >= 0 ? ticket.id : 'new' ])
    );
  }

  ngOnInit() {
  }

}
