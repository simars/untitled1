import {AfterViewInit, Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {fromEvent, Observable, Subscription} from 'rxjs';
import {combineLatest, debounceTime, distinctUntilChanged, map, startWith, tap} from 'rxjs/operators';

export interface TicketSearchCriterion {
  term: string;
  completedOnly: boolean;
}

@Component({
  selector: 'app-ticket-search-box',
  templateUrl: 'ticket-search-box.component.html',
  styleUrls: ['./ticket-search-box.component.css']
})
export class TicketSearchBoxComponent implements AfterViewInit, OnDestroy {

  @Output('onSearch') onSearch = new EventEmitter<TicketSearchCriterion>();

  @ViewChild('searchInput') input: ElementRef;
  @ViewChild('completedOnly') completedOnly: ElementRef;

  private subscription: Subscription;


  ngAfterViewInit(): void {
    const completed$ = fromEvent<any>(this.completedOnly.nativeElement, 'input').pipe(
      map(($event) => $event.target.checked),
      startWith(false)
    );

    const term$ = fromEvent<any>(this.input.nativeElement, 'keyup')
      .pipe(
        map(event => event.target.value),
        startWith(''),
        debounceTime(400),
        distinctUntilChanged()
      );

    this.subscription = term$
      .pipe(
        combineLatest(completed$),
        map(([term, completedOnly]) => ({term, completedOnly})),
      ).subscribe(
        criterion => {
          this.onSearch.emit(criterion);
        }
      );

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }



}
