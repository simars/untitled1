import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketSearchBoxComponent } from './ticket-search-box.component';

describe('TicketSearchBoxComponent', () => {
  let component: TicketSearchBoxComponent;
  let fixture: ComponentFixture<TicketSearchBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketSearchBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketSearchBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
