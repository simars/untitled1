import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {RouterModule} from '@angular/router';
import {TicketListComponent} from './pages/ticket-list/ticket-list.component';
import { TicketDetailComponent } from './pages/ticket-detail/ticket-detail.component';
import { ReactiveFormsModule} from '@angular/forms';
import {NgrxAppModule} from './reducers';
import { TicketContainerComponent } from './pages/ticket-container/ticket-container.component';
import { TicketSearchBoxComponent } from './pages/ticket-search-box/ticket-search-box.component';

@NgModule({
  declarations: [
    AppComponent,
    TicketListComponent,
    TicketDetailComponent,
    TicketContainerComponent,
    TicketSearchBoxComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'tickets',
      },
      {
        path: 'tickets',
        component: TicketContainerComponent
      },
      {
        path: 'ticket/:id',
        component: TicketDetailComponent
      },
      {
        path: '**',
        redirectTo: 'tickets'
      }
    ]),
    NgrxAppModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

}
